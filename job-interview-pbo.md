# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)


Penggunaan matematika dan algoritma pemrograman dalam kodingan di bawah memungkinkan untuk menyelesaikan masalah cek saldo, pembayaran ewallet, pembayaran tagihan, dan tranfer dana.
> Usecase Penyimpanan dimana dapat cek saldo, pembayaran ewallet, pembayaran tagihan, dan tranfer dana

- cek saldo
```
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

// Kelas Rekening
class Rekening {
    protected  String nomorRekening;
    private String pin;
    private double saldo;
    private Map<String, Double> tagihan;
    private List<String> riwayatTransfer;

    public Rekening(String nomorRekening, String pin) {
        this.nomorRekening = nomorRekening;
        this.pin = pin;
        this.saldo = 1000000; // Saldo awal
        this.tagihan = new HashMap<>();
        this.riwayatTransfer = new ArrayList<>();
    }

    public boolean login(String pin) {
        return this.pin.equals(pin);
    }

    public double getSaldo() {
        return saldo;
    }
//cek saldo
    public void cekSaldo(String namaNasabah) {
        System.out.println("Saldo " + namaNasabah + " adalah Rp " + saldo);
    }
```

- tranfer

method berikut berfungsi untuk menginput jumlah tranfer
```
public static void transfer(Scanner scanner, Nasabah pengirim) {
        System.out.print("Masukkan nomor rekening tujuan: ");
        String nomorRekeningTujuan = scanner.nextLine();

        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        System.out.print("Masukkan nama penerima: ");
        String namaPenerima = scanner.nextLine();
        System.out.println();

        if (pengirim.transfer(nomorRekeningTujuan, jumlahTransfer)) {
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " atas nama " + namaPenerima + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer gagal.");
        }
    }
```
method dibawah ini berfungsi untuk menampilkan Fungsi diatas bahwa transfer telah terkirim sebanyak yang diinput oleh user
```
public boolean transfer(String nomorRekeningTujuan, double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            String riwayat = "Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan;
            riwayatTransfer.add(riwayat);
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " berhasil dilakukan.");
            System.out.println("Sisa saldo Anda adalah Rp " + saldo);
            return true;
        } else {
            System.out.println("Saldo tidak mencukupi.");
            return false;
        }
```

- E-wallet

didalam menu ewallet kita dapat melakukan tranfer via ovo dan dana
```
 public static void eWallet(Nasabah nasabah) {
        System.out.println("Menu E-Wallet:");
        System.out.println("1. Transfer via OVO");
        System.out.println("2. Transfer via Dana");
        System.out.println("0. Kembali ke menu utama");
        System.out.print("Pilih metode transfer (0-2): ");
    
        Scanner scanner = new Scanner(System.in);
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        switch (pilihan) {
            case 0:
                return;
            case 1:
                transferViaOVO(scanner, nasabah);
                break;
            case 2:
                transferViaDana(scanner, nasabah);
                break;
            default:
                System.out.println("Metode transfer tidak valid.");
                break;
        }
    }
    
    public static void transferViaOVO(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        if (nasabah.transfer(nasabah.getNomorRekening(), jumlahTransfer)) {
            System.out.println("Transfer via OVO sebesar " + jumlahTransfer + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer via OVO gagal.");
        }
    }
    
    public static void transferViaDana(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        if (nasabah.transfer(nasabah.getNomorRekening(), jumlahTransfer)) {
            System.out.println("Transfer via Dana sebesar " + jumlahTransfer + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer via Dana gagal.");
        }
    }
```


- isi saldo

method berikut berfungsi untuk menginput jumlah saldo

 ```
public static void isiSaldo(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah saldo yang ingin diisi: ");
        double jumlahIsiSaldo = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        nasabah.isiSaldo(jumlahIsiSaldo);
    }
```

method dibawah ini berfungsi untuk menampilkan Fungsi diatas bahwa saldo telah di isi sebanyak yang diinput oleh user

```
 public void isiSaldo(double jumlahIsiSaldo) {
        saldo += jumlahIsiSaldo;
        System.out.println("Saldo Anda berhasil ditambahkan sebesar " + jumlahIsiSaldo);
        System.out.println("Saldo saat ini adalah Rp " + saldo);
    }
```


- Tagihan

method dibawah ini berisi menu bayarTagihan,tambahTagihan,tampilkanTagihan
```
public static void tagihan(Scanner scanner, Nasabah nasabah) {
        System.out.println("Menu Tagihan:");
        System.out.println("1. Bayar Tagihan");
        System.out.println("2. Tambah Tagihan");
        System.out.println("3. Tampilkan Tagihan"); // Tambahkan opsi ini
        System.out.println("0. Kembali ke menu utama");
        System.out.print("Pilih menu tagihan (0-3): ");
    
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        switch (pilihan) {
            case 0:
                return;
            case 1:
                bayarTagihan(scanner, nasabah);
                break;
            case 2:
                tambahTagihan(scanner, nasabah);
                break;
            case 3:
                tampilkanTagihan(nasabah); // Panggil metode tampilkanTagihan
                break;
            default:
                System.out.println("Menu tagihan tidak valid.");
                break;
        }
    }
    

    public static void bayarTagihan(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan nama tagihan yang ingin dibayar: ");
        String namaTagihan = scanner.nextLine();

        if (nasabah.bayarTagihan(namaTagihan)) {
            System.out.println("Pembayaran tagihan " + namaTagihan + " berhasil dilakukan.");
        } else {
            System.out.println("Pembayaran tagihan " + namaTagihan + " gagal.");
        }
    }

    public static void tambahTagihan(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan nama tagihan: ");
        String namaTagihan = scanner.nextLine();

        System.out.print("Masukkan jumlah tagihan: ");
        double jumlahTagihan = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        nasabah.tambahTagihan(namaTagihan, jumlahTagihan);
    }
    public static void tampilkanTagihan(Nasabah nasabah) {
        List<String> daftarTagihan = nasabah.getDaftarTagihan();
    
        if (daftarTagihan.isEmpty()) {
            System.out.println("Tidak ada tagihan yang tersedia.");
        } else {
            System.out.println("Daftar Tagihan:");
            for (String namaTagihan : daftarTagihan) {
                System.out.println("- " + namaTagihan);
            }
        }
    }
```

method dibawah ini berfungsi untuk menampilkan info tagihan
```
public boolean bayarTagihan(String namaTagihan) {
        if (tagihan.containsKey(namaTagihan)) {
            double jumlahTagihan = tagihan.get(namaTagihan);
            if (saldo >= jumlahTagihan) {
                saldo -= jumlahTagihan;
                tagihan.remove(namaTagihan);
                System.out.println("Pembayaran tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " berhasil dilakukan.");
                System.out.println("Sisa saldo Anda adalah Rp " + saldo);
                return true;
            } else {
                System.out.println("Saldo tidak mencukupi untuk membayar tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan);
                return false;
            }
        } else {
            System.out.println("Tagihan " + namaTagihan + " tidak ditemukan.");
            return false;
        }
    }

    public void tambahTagihan(String namaTagihan, double jumlahTagihan) {
        tagihan.put(namaTagihan, jumlahTagihan);
        System.out.println("Tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " telah ditambahkan.");
    }

    public List<String> getDaftarTagihan() {
        return new ArrayList<>(tagihan.keySet());
    }

    public List<String> getRiwayatTransfer() {
        return new ArrayList<>(riwayatTransfer);
    }
}
```

- Riwayat Transfer
```
    public static void tampilkanRiwayatTransfer(Nasabah nasabah) {
        List<String> riwayatTransfer = nasabah.getRiwayatTransfer();

        if (riwayatTransfer.isEmpty()) {
            System.out.println("Belum ada riwayat transfer.");
        } else {
            System.out.println("Riwayat Transfer:");
            for (String riwayat : riwayatTransfer) {
                System.out.println("- " + riwayat);
            }
        }
    }
```



# 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Penggunaan algoritma pada kodingan di bawah menyelesaikan masalah cek saldo, pembayaran ewallet, pembayaran tagihan, dan tranfer dana.

1. buat kelas Rekening

```
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

// Kelas Rekening
class Rekening {
    protected  String nomorRekening;
    private String pin;
    private double saldo;
    private Map<String, Double> tagihan;
    private List<String> riwayatTransfer;

    public Rekening(String nomorRekening, String pin) {
        this.nomorRekening = nomorRekening;
        this.pin = pin;
        this.saldo = 1000000; // Saldo awal
        this.tagihan = new HashMap<>();
        this.riwayatTransfer = new ArrayList<>();
    }

    public boolean login(String pin) {
        return this.pin.equals(pin);
    }

    public double getSaldo() {
        return saldo;
    }
//cek saldo
    public void cekSaldo(String namaNasabah) {
        System.out.println("Saldo " + namaNasabah + " adalah Rp " + saldo);
    }
//transfer i (info)
    public boolean transfer(String nomorRekeningTujuan, double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            String riwayat = "Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan;
            riwayatTransfer.add(riwayat);
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " berhasil dilakukan.");
            System.out.println("Sisa saldo Anda adalah Rp " + saldo);
            return true;
        } else {
            System.out.println("Saldo tidak mencukupi.");
            return false;
        }
    }
// isi saldo i (info)
    public void isiSaldo(double jumlahIsiSaldo) {
        saldo += jumlahIsiSaldo;
        System.out.println("Saldo Anda berhasil ditambahkan sebesar " + jumlahIsiSaldo);
        System.out.println("Saldo saat ini adalah Rp " + saldo);
    }
// tagihan i (info)
    public boolean bayarTagihan(String namaTagihan) {
        if (tagihan.containsKey(namaTagihan)) {
            double jumlahTagihan = tagihan.get(namaTagihan);
            if (saldo >= jumlahTagihan) {
                saldo -= jumlahTagihan;
                tagihan.remove(namaTagihan);
                System.out.println("Pembayaran tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " berhasil dilakukan.");
                System.out.println("Sisa saldo Anda adalah Rp " + saldo);
                return true;
            } else {
                System.out.println("Saldo tidak mencukupi untuk membayar tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan);
                return false;
            }
        } else {
            System.out.println("Tagihan " + namaTagihan + " tidak ditemukan.");
            return false;
        }
    }

    public void tambahTagihan(String namaTagihan, double jumlahTagihan) {
        tagihan.put(namaTagihan, jumlahTagihan);
        System.out.println("Tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " telah ditambahkan.");
    }

    public List<String> getDaftarTagihan() {
        return new ArrayList<>(tagihan.keySet());
    }

    public List<String> getRiwayatTransfer() {
        return new ArrayList<>(riwayatTransfer);
    }
}
```


2. buat Kelas Nasabah (Turunan dari kelas Rekening)

```
class Nasabah extends Rekening {
    private String nama;
    private String alamat;

    public Nasabah(String nomorRekening, String pin, String nama, String alamat) {
        super(nomorRekening, pin);
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }
    

    public String getAlamat() {
        return alamat;
    }
    
    public String getNomorRekening() {
        return nomorRekening;
    }
    public boolean transferViaDana(double jumlahTransfer) {
        if (saldoUtama >= jumlahTransfer) {
            saldoUtama -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }
    
    public boolean transferViaOVO(double jumlahTransfer) {
        if (saldoUtama >= jumlahTransfer) {
            saldoUtama -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }
}
```


3. buat kelas main yaitu BcaMobile

```
public class BCAMobile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat datang di BCA Mobile!");
        System.out.print("Masukkan nomor rekening: ");
        String nomorRekening = scanner.nextLine();

        System.out.print("Masukkan PIN: ");
        String pin = scanner.nextLine();

        Nasabah nasabah = new Nasabah(nomorRekening, pin, "Diaz", "Jl. Permata Biru No. 123");

        if (nasabah.login(pin)) {
            System.out.println("Login berhasil!");
            System.out.println("Selamat datang, " + nasabah.getNama() + "!");
            boolean running = true;
            while (running) {
                System.out.println("Menu:");
                System.out.println("1. Cek Saldo");
                System.out.println("2. Transfer");
                System.out.println("3. E-Wallet");
                System.out.println("4. Isi Saldo");
                System.out.println("5. Tagihan");
                System.out.println("6. Tampilkan Riwayat Transfer");
                System.out.println("0. Keluar");
                System.out.print("Pilih menu (0-6): ");
                try {
                    int pilihan = scanner.nextInt();
                    scanner.nextLine(); // Membersihkan newline character setelah membaca angka

                    switch (pilihan) {
                        case 0:
                            running = false;
                            break;
                        case 1:
                            nasabah.cekSaldo(nasabah.getNama());
                            break;
                        case 2:
                            transfer(scanner, nasabah);
                            break;
                        case 3:
                            eWallet(nasabah);
                            break;
                        case 4:
                            isiSaldo(scanner, nasabah);
                            break;
                        case 5:
                            tagihan(scanner, nasabah);
                            break;
                        case 6:
                            tampilkanRiwayatTransfer(nasabah);
                            break;
                        default:
                            System.out.println("Menu tidak valid.");
                            break;
                    }
                } catch (NoSuchElementException e) {
                    System.out.println("Input tidak valid. Mohon masukkan angka yang sesuai dengan pilihan menu.");
                    scanner.nextLine(); // Membersihkan input yang tidak valid
                }
            }
        } else {
            System.out.println("Login gagal. Nomor rekening atau PIN salah.");
        }

        scanner.close();
    }
```


4. Selanjutnya membuat method untuk cekSaldo,tranfer,ewallet,isiSaldo,tagihan,riwayatTransfer

```
// transfer ii (input)
    public static void transfer(Scanner scanner, Nasabah pengirim) {
        System.out.print("Masukkan nomor rekening tujuan: ");
        String nomorRekeningTujuan = scanner.nextLine();

        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        System.out.print("Masukkan nama penerima: ");
        String namaPenerima = scanner.nextLine();
        System.out.println();

        if (pengirim.transfer(nomorRekeningTujuan, jumlahTransfer)) {
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " atas nama " + namaPenerima + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer gagal.");
        }
    }
//ewallet
    public static void eWallet(Nasabah nasabah) {
        System.out.println("Menu E-Wallet:");
        System.out.println("1. Transfer via OVO");
        System.out.println("2. Transfer via Dana");
        System.out.println("0. Kembali ke menu utama");
        System.out.print("Pilih metode transfer (0-2): ");
    
        Scanner scanner = new Scanner(System.in);
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        switch (pilihan) {
            case 0:
                return;
            case 1:
                transferViaOVO(scanner, nasabah);
                break;
            case 2:
                transferViaDana(scanner, nasabah);
                break;
            default:
                System.out.println("Metode transfer tidak valid.");
                break;
        }
    }
    
    public static void transferViaOVO(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        if (nasabah.transfer(nasabah.getNomorRekening(), jumlahTransfer)) {
            System.out.println("Transfer via OVO sebesar " + jumlahTransfer + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer via OVO gagal.");
        }
    }
    
    public static void transferViaDana(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        if (nasabah.transfer(nasabah.getNomorRekening(), jumlahTransfer)) {
            System.out.println("Transfer via Dana sebesar " + jumlahTransfer + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer via Dana gagal.");
        }
    }
//isi saldo ii (input)
    public static void isiSaldo(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah saldo yang ingin diisi: ");
        double jumlahIsiSaldo = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        nasabah.isiSaldo(jumlahIsiSaldo);
    }
//tagihan ii (input)
    public static void tagihan(Scanner scanner, Nasabah nasabah) {
        System.out.println("Menu Tagihan:");
        System.out.println("1. Bayar Tagihan");
        System.out.println("2. Tambah Tagihan");
        System.out.println("3. Tampilkan Tagihan"); // Tambahkan opsi ini
        System.out.println("0. Kembali ke menu utama");
        System.out.print("Pilih menu tagihan (0-3): ");
    
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        switch (pilihan) {
            case 0:
                return;
            case 1:
                bayarTagihan(scanner, nasabah);
                break;
            case 2:
                tambahTagihan(scanner, nasabah);
                break;
            case 3:
                tampilkanTagihan(nasabah); // Panggil metode tampilkanTagihan
                break;
            default:
                System.out.println("Menu tagihan tidak valid.");
                break;
        }
    }
    

    public static void bayarTagihan(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan nama tagihan yang ingin dibayar: ");
        String namaTagihan = scanner.nextLine();

        if (nasabah.bayarTagihan(namaTagihan)) {
            System.out.println("Pembayaran tagihan " + namaTagihan + " berhasil dilakukan.");
        } else {
            System.out.println("Pembayaran tagihan " + namaTagihan + " gagal.");
        }
    }

    public static void tambahTagihan(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan nama tagihan: ");
        String namaTagihan = scanner.nextLine();

        System.out.print("Masukkan jumlah tagihan: ");
        double jumlahTagihan = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        nasabah.tambahTagihan(namaTagihan, jumlahTagihan);
    }
    public static void tampilkanTagihan(Nasabah nasabah) {
        List<String> daftarTagihan = nasabah.getDaftarTagihan();
    
        if (daftarTagihan.isEmpty()) {
            System.out.println("Tidak ada tagihan yang tersedia.");
        } else {
            System.out.println("Daftar Tagihan:");
            for (String namaTagihan : daftarTagihan) {
                System.out.println("- " + namaTagihan);
            }
        }
    }
//riwayat tranfer
    public static void tampilkanRiwayatTransfer(Nasabah nasabah) {
        List<String> riwayatTransfer = nasabah.getRiwayatTransfer();

        if (riwayatTransfer.isEmpty()) {
            System.out.println("Belum ada riwayat transfer.");
        } else {
            System.out.println("Riwayat Transfer:");
            for (String riwayat : riwayatTransfer) {
                System.out.println("- " + riwayat);
            }
        }
    }
}
```


# 3. Mampu menjelaskan konsep dasar OOP

Konsep dasar OOP (Object-Oriented Programming) adalah sebuah pemrograman yang fokus pada objek atau entitas dalam program yang memiliki atribut dan perilaku tertentu.
Objek dalam OOP memiliki beberapa karakteristik (4 pilar) seperti:


Encapsulation: Kemampuan untuk menyembunyikan data dan implementasi dalam sebuah objek dan hanya memberikan akses melalui metode atau antarmuka yang didefinisikan.


Inheritance: Kemampuan untuk membuat objek baru yang memiliki karakteristik dari objek yang sudah ada, sehingga memungkinkan penggunaan kembali kode dan mempermudah pengorganisasian kode.


Polymorphism: Kemampuan untuk memungkinkan objek untuk memiliki banyak bentuk atau perilaku yang berbeda tergantung pada konteks penggunaannya.


Abstraction: kemampuan untuk mengisolasi detail yang tidak diperlukan dan hanya mengekspos informasi yang relevan atau penting.

Fungsi OOP dalam industri
OOP sangat penting di perusahaan karena menurut saya dari cara kerjanya yang mudah dipahami dan terstruktur dalam membuat program. OOP membantu kita mengatur kode program dengan rapi, mirip seperti mengelompokkan benda-benda dalam kotak.

# 4.Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)


Pada contoh di bawah ini, atribut nomorRekening,pin,saldo,tagihan dalam kelas rekening memiliki modifier private dan protected lalu diakses melalui metode getter dan setter. Hal ini menerapkan pilar Encapsulation.

```
abstract class Rekening {
    protected String nomorRekening;
    private String pin;
    protected double saldo;
    protected Map<String, Double> tagihan;
    protected List<String> riwayatTransfer;
```

# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

```
abstract class Rekening {
    protected String nomorRekening;
    private String pin;
    protected double saldo;
    protected Map<String, Double> tagihan;
    protected List<String> riwayatTransfer;

    public Rekening(String nomorRekening, String pin) {
        this.nomorRekening = nomorRekening;
        this.pin = pin;
        this.saldo = 1000000; // Saldo awal
        this.tagihan = new HashMap<>();
        this.riwayatTransfer = new ArrayList<>();
    }

    public boolean login(String pin) {
        return this.pin.equals(pin);
    }

    public double getSaldo() {
        return saldo;
    }

    public abstract void cekSaldo(String namaNasabah);

    public abstract boolean transfer(String nomorRekeningTujuan, double jumlahTransfer);

    public abstract void isiSaldo(double jumlahIsiSaldo);

    public abstract boolean bayarTagihan(String namaTagihan);

    public void tambahTagihan(String namaTagihan, double jumlahTagihan) {
        tagihan.put(namaTagihan, jumlahTagihan);
        System.out.println("Tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " telah ditambahkan.");
    }

    public List<String> getDaftarTagihan() {
        return new ArrayList<>(tagihan.keySet());
    }

    public List<String> getRiwayatTransfer() {
        return new ArrayList<>(riwayatTransfer);
    }
}

class Nasabah extends Rekening {
    private String nama;
    private String alamat;

    public Nasabah(String nomorRekening, String pin, String nama, String alamat) {
        super(nomorRekening, pin);
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    @Override
    public void cekSaldo(String namaNasabah) {
        System.out.println("Saldo " + namaNasabah + " adalah Rp " + saldo);
    }

    @Override
    public boolean transfer(String nomorRekeningTujuan, double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            String riwayat = "Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan;
            riwayatTransfer.add(riwayat);
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " berhasil dilakukan.");
            System.out.println("Sisa saldo Anda adalah Rp " + saldo);
            return true;
        } else {
            System.out.println("Saldo tidak mencukupi.");
            return false;
        }
    }

    @Override
    public void isiSaldo(double jumlahIsiSaldo) {
        saldo += jumlahIsiSaldo;
        System.out.println("Saldo Anda berhasil ditambahkan sebesar " + jumlahIsiSaldo);
        System.out.println("Saldo saat ini adalah Rp " + saldo);
    }

    @Override
    public boolean bayarTagihan(String namaTagihan) {
        if (tagihan.containsKey(namaTagihan)) {
            double jumlahTagihan = tagihan.get(namaTagihan);
            if (saldo >= jumlahTagihan) {
                saldo -= jumlahTagihan;
                tagihan.remove(namaTagihan);
                System.out.println("Pembayaran tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " berhasil dilakukan.");
                System.out.println("Sisa saldo Anda adalah Rp " + saldo);
                return true;
            } else {
                System.out.println("Saldo tidak mencukupi untuk membayar tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan);
                return false;
            }
        } else {
            System.out.println("Tagihan " + namaTagihan + " tidak ditemukan.");
            return false;
        }
    }
}
```


Kelas abstrak tidak dapat diinstansiasi langsung, tetapi digunakan sebagai kerangka dasar untuk kelas turunannya.

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

- Inheritance

Kelas Nasabah merupakan turunan dari kelas Rekening. Dengan menggunakan kata kunci extends, kelas Nasabah mewarisi semua atribut dan metode yang ada di kelas Rekening. 

```
class Nasabah extends Rekening {
    private String nama;
    private String alamat;

    public Nasabah(String nomorRekening, String pin, String nama, String alamat) {
        super(nomorRekening, pin);
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getNomorRekening() {
        return nomorRekening;
    }

    public boolean transferViaDana(double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }

    public boolean transferViaOVO(double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }
```
Hal ini memungkinkan kelas Nasabah untuk memperluas dan mengkhususkan fungsionalitas yang ada, serta menambahkan metode dan variabel baru sesuai kebutuhan.

- Polymorphism

Terdapat metode-metode yang di-override di kelas Nasabah untuk mengimplementasikan fungsionalitas yang spesifik. Misalnya, metode transfer diubah dan diperluas di kelas Nasabah untuk mencatat riwayat transfer dan menampilkan informasi yang sesuai.

```
class Nasabah extends Rekening {
    private String nama;
    private String alamat;

    public Nasabah(String nomorRekening, String pin, String nama, String alamat) {
        super(nomorRekening, pin);
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getNomorRekening() {
        return nomorRekening;
    }

    public boolean transferViaDana(double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }

    public boolean transferViaOVO(double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void cekSaldo(String namaNasabah) {
        System.out.println("Saldo " + namaNasabah + " adalah Rp " + saldo);
    }

    @Override
    public boolean transfer(String nomorRekeningTujuan, double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            String riwayat = "Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan;
            riwayatTransfer.add(riwayat);
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " berhasil dilakukan.");
            System.out.println("Sisa saldo Anda adalah Rp " + saldo);
            return true;
        } else {
            System.out.println("Saldo tidak mencukupi.");
            return false;
        }
    }

    @Override
    public void isiSaldo(double jumlahIsiSaldo) {
        saldo += jumlahIsiSaldo;
        System.out.println("Saldo Anda berhasil ditambahkan sebesar " + jumlahIsiSaldo);
        System.out.println("Saldo saat ini adalah Rp " + saldo);
    }

    @Override
    public boolean bayarTagihan(String namaTagihan) {
        if (tagihan.containsKey(namaTagihan)) {
            double jumlahTagihan = tagihan.get(namaTagihan);
            if (saldo >= jumlahTagihan) {
                saldo -= jumlahTagihan;
                tagihan.remove(namaTagihan);
                System.out.println("Pembayaran tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " berhasil dilakukan.");
                System.out.println("Sisa saldo Anda adalah Rp " + saldo);
                return true;
            } else {
                System.out.println("Saldo tidak mencukupi untuk membayar tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan);
                return false;
            }
        } else {
            System.out.println("Tagihan " + namaTagihan + " tidak ditemukan.");
            return false;
        }
    }



    public void tambahTagihan(String namaTagihan, double jumlahTagihan) {
        tagihan.put(namaTagihan, jumlahTagihan);
        System.out.println("Tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " telah ditambahkan.");
    }

    public List<String> getDaftarTagihan() {
        return new ArrayList<>(tagihan.keySet());
    }

    public List<String> getRiwayatTransfer() {
        return new ArrayList<>(riwayatTransfer);
    }
}

// Kelas Nasabah (Turunan dari kelas Rekening)
class Nasabah extends Rekening {
    private String nama;
    private String alamat;

    public Nasabah(String nomorRekening, String pin, String nama, String alamat) {
        super(nomorRekening, pin);
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }
    

    public String getAlamat() {
        return alamat;
    }
    
    public String getNomorRekening() {
        return nomorRekening;
    }
    public boolean transferViaDana(double jumlahTransfer) {
        if (saldoUtama >= jumlahTransfer) {
            saldoUtama -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }
    
    public boolean transferViaOVO(double jumlahTransfer) {
        if (saldoUtama >= jumlahTransfer) {
            saldoUtama -= jumlahTransfer;
            return true;
        } else {
            return false;
        }
    }
}
```

# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Langkah-langkah untuk mendeskripsikan proses bisnis ke dalam OOP yaitu:

Identifikasi objek-objek yang terlibat:

Dalam setiap use case, terdapat objek-objek yang terlibat dalam proses bisnis. Identifikasi objek-objek tersebut berdasarkan fitur-fitur yang ada dalam setiap use case.

Tentukan kelas-kelas:

Setelah mengidentifikasi objek-objek, tentukan kelas-kelas yang mewakili objek-objek tersebut. Setiap kelas akan memiliki atribut dan metode yang sesuai dengan fungsionalitasnya.

Definisikan atribut dan metode:

Untuk setiap kelas, definisikan atribut-atribut yang diperlukan untuk menyimpan informasi yang relevan. Misalnya, untuk kelas Rekening, atribut dapat berupa nama nasabah, daftar tranfer, dan jumlah saldo.

Selanjutnya, definisikan metode-metode yang akan mengimplementasikan fungsionalitas yang terkait dengan setiap use case. Misalnya, untuk kelas Rekening, metode-metode dapat berupa

```
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

abstract class Rekening {
    protected String nomorRekening;
    private String pin;
    protected double saldo;
    protected Map<String, Double> tagihan;
    protected List<String> riwayatTransfer;

    public Rekening(String nomorRekening, String pin) {
        this.nomorRekening = nomorRekening;
        this.pin = pin;
        this.saldo = 1000000; // Saldo awal
        this.tagihan = new HashMap<>();
        this.riwayatTransfer = new ArrayList<>();
    }

    public boolean login(String pin) {
        return this.pin.equals(pin);
    }

    public double getSaldo() {
        return saldo;
    }

    public abstract void cekSaldo(String namaNasabah);

    public abstract boolean transfer(String nomorRekeningTujuan, double jumlahTransfer);

    public abstract void isiSaldo(double jumlahIsiSaldo);

    public abstract boolean bayarTagihan(String namaTagihan);

    public void tambahTagihan(String namaTagihan, double jumlahTagihan) {
        tagihan.put(namaTagihan, jumlahTagihan);
        System.out.println("Tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " telah ditambahkan.");
    }

    public List<String> getDaftarTagihan() {
        return new ArrayList<>(tagihan.keySet());
    }

    public List<String> getRiwayatTransfer() {
        return new ArrayList<>(riwayatTransfer);
    }
}

class Nasabah extends Rekening {
    private String nama;
    private String alamat;

    public Nasabah(String nomorRekening, String pin, String nama, String alamat) {
        super(nomorRekening, pin);
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    @Override
    public void cekSaldo(String namaNasabah) {
        System.out.println("Saldo " + namaNasabah + " adalah Rp " + saldo);
    }

    @Override
    public boolean transfer(String nomorRekeningTujuan, double jumlahTransfer) {
        if (saldo >= jumlahTransfer) {
            saldo -= jumlahTransfer;
            String riwayat = "Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan;
            riwayatTransfer.add(riwayat);
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " berhasil dilakukan.");
            System.out.println("Sisa saldo Anda adalah Rp " + saldo);
            return true;
        } else {
            System.out.println("Saldo tidak mencukupi.");
            return false;
        }
    }

    @Override
    public void isiSaldo(double jumlahIsiSaldo) {
        saldo += jumlahIsiSaldo;
        System.out.println("Saldo Anda berhasil ditambahkan sebesar " + jumlahIsiSaldo);
        System.out.println("Saldo saat ini adalah Rp " + saldo);
    }

    @Override
    public boolean bayarTagihan(String namaTagihan) {
        if (tagihan.containsKey(namaTagihan)) {
            double jumlahTagihan = tagihan.get(namaTagihan);
            if (saldo >= jumlahTagihan) {
                saldo -= jumlahTagihan;
                tagihan.remove(namaTagihan);
                System.out.println("Pembayaran tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan + " berhasil dilakukan.");
                System.out.println("Sisa saldo Anda adalah Rp " + saldo);
                return true;
            } else {
                System.out.println("Saldo tidak mencukupi untuk membayar tagihan " + namaTagihan + " sebesar Rp " + jumlahTagihan);
                return false;
            }
        } else {
            System.out.println("Tagihan " + namaTagihan + " tidak ditemukan.");
            return false;
        }
    }
}
// Kelas BCAMobile
public class BCAMobile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat datang di BCA Mobile!");
        System.out.print("Masukkan nomor rekening: ");
        String nomorRekening = scanner.nextLine();

        System.out.print("Masukkan PIN: ");
        String pin = scanner.nextLine();

        Nasabah nasabah = new Nasabah(nomorRekening, pin, "Diaz", "Jl. Permata Biru No. 123");

        if (nasabah.login(pin)) {
            System.out.println("Login berhasil!");
            System.out.println("Selamat datang, " + nasabah.getNama() + "!");
            boolean running = true;
            while (running) {
                System.out.println("Menu:");
                System.out.println("1. Cek Saldo");
                System.out.println("2. Transfer");
                System.out.println("3. E-Wallet");
                System.out.println("4. Isi Saldo");
                System.out.println("5. Tagihan");
                System.out.println("6. Tampilkan Riwayat Transfer");
                System.out.println("0. Keluar");
                System.out.print("Pilih menu (0-6): ");
                try {
                    int pilihan = scanner.nextInt();
                    scanner.nextLine(); // Membersihkan newline character setelah membaca angka

                    switch (pilihan) {
                        case 0:
                            running = false;
                            break;
                        case 1:
                            nasabah.cekSaldo(nasabah.getNama());
                            break;
                        case 2:
                            transfer(scanner, nasabah);
                            break;
                        case 3:
                            eWallet(nasabah);
                            break;
                        case 4:
                            isiSaldo(scanner, nasabah);
                            break;
                        case 5:
                            tagihan(scanner, nasabah);
                            break;
                        case 6:
                            tampilkanRiwayatTransfer(nasabah);
                            break;
                        default:
                            System.out.println("Menu tidak valid.");
                            break;
                    }
                } catch (NoSuchElementException e) {
                    System.out.println("Input tidak valid. Mohon masukkan angka yang sesuai dengan pilihan menu.");
                    scanner.nextLine(); // Membersihkan input yang tidak valid
                }
            }
        } else {
            System.out.println("Login gagal. Nomor rekening atau PIN salah.");
        }

        scanner.close();
    }
// transfer ii (input)
    public static void transfer(Scanner scanner, Nasabah pengirim) {
        System.out.print("Masukkan nomor rekening tujuan: ");
        String nomorRekeningTujuan = scanner.nextLine();

        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        System.out.print("Masukkan nama penerima: ");
        String namaPenerima = scanner.nextLine();
        System.out.println();

        if (pengirim.transfer(nomorRekeningTujuan, jumlahTransfer)) {
            System.out.println("Transfer sebesar " + jumlahTransfer + " ke nomor rekening " + nomorRekeningTujuan + " atas nama " + namaPenerima + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer gagal.");
        }
    }
//ewallet
    public static void eWallet(Nasabah nasabah) {
        System.out.println("Menu E-Wallet:");
        System.out.println("1. Transfer via OVO");
        System.out.println("2. Transfer via Dana");
        System.out.println("0. Kembali ke menu utama");
        System.out.print("Pilih metode transfer (0-2): ");
    
        Scanner scanner = new Scanner(System.in);
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        switch (pilihan) {
            case 0:
                return;
            case 1:
                transferViaOVO(scanner, nasabah);
                break;
            case 2:
                transferViaDana(scanner, nasabah);
                break;
            default:
                System.out.println("Metode transfer tidak valid.");
                break;
        }
    }
    
    public static void transferViaOVO(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        if (nasabah.transfer(nasabah.getNomorRekening(), jumlahTransfer)) {
            System.out.println("Transfer via OVO sebesar " + jumlahTransfer + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer via OVO gagal.");
        }
    }
    
    public static void transferViaDana(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah transfer: ");
        double jumlahTransfer = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        if (nasabah.transfer(nasabah.getNomorRekening(), jumlahTransfer)) {
            System.out.println("Transfer via Dana sebesar " + jumlahTransfer + " berhasil dilakukan.");
        } else {
            System.out.println("Transfer via Dana gagal.");
        }
    }
//isi saldo ii (input)
    public static void isiSaldo(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan jumlah saldo yang ingin diisi: ");
        double jumlahIsiSaldo = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        nasabah.isiSaldo(jumlahIsiSaldo);
    }
//tagihan ii (input)
    public static void tagihan(Scanner scanner, Nasabah nasabah) {
        System.out.println("Menu Tagihan:");
        System.out.println("1. Bayar Tagihan");
        System.out.println("2. Tambah Tagihan");
        System.out.println("3. Tampilkan Tagihan"); // Tambahkan opsi ini
        System.out.println("0. Kembali ke menu utama");
        System.out.print("Pilih menu tagihan (0-3): ");
    
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka
    
        switch (pilihan) {
            case 0:
                return;
            case 1:
                bayarTagihan(scanner, nasabah);
                break;
            case 2:
                tambahTagihan(scanner, nasabah);
                break;
            case 3:
                tampilkanTagihan(nasabah); // Panggil metode tampilkanTagihan
                break;
            default:
                System.out.println("Menu tagihan tidak valid.");
                break;
        }
    }
    

    public static void bayarTagihan(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan nama tagihan yang ingin dibayar: ");
        String namaTagihan = scanner.nextLine();

        if (nasabah.bayarTagihan(namaTagihan)) {
            System.out.println("Pembayaran tagihan " + namaTagihan + " berhasil dilakukan.");
        } else {
            System.out.println("Pembayaran tagihan " + namaTagihan + " gagal.");
        }
    }

    public static void tambahTagihan(Scanner scanner, Nasabah nasabah) {
        System.out.print("Masukkan nama tagihan: ");
        String namaTagihan = scanner.nextLine();

        System.out.print("Masukkan jumlah tagihan: ");
        double jumlahTagihan = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline character setelah membaca angka

        nasabah.tambahTagihan(namaTagihan, jumlahTagihan);
    }
    public static void tampilkanTagihan(Nasabah nasabah) {
        List<String> daftarTagihan = nasabah.getDaftarTagihan();
    
        if (daftarTagihan.isEmpty()) {
            System.out.println("Tidak ada tagihan yang tersedia.");
        } else {
            System.out.println("Daftar Tagihan:");
            for (String namaTagihan : daftarTagihan) {
                System.out.println("- " + namaTagihan);
            }
        }
    }
//riwayat tranfer
    public static void tampilkanRiwayatTransfer(Nasabah nasabah) {
        List<String> riwayatTransfer = nasabah.getRiwayatTransfer();

        if (riwayatTransfer.isEmpty()) {
            System.out.println("Belum ada riwayat transfer.");
        } else {
            System.out.println("Riwayat Transfer:");
            for (String riwayat : riwayatTransfer) {
                System.out.println("- " + riwayat);
            }
        }
    }
}
```


Hubungkan antara kelas:
Identifikasi Hubungan antara kelas kelas yang ada misalnya kelas saldo melalui asosiasi dimana nasabah memiliki objek saldo yang ada di dalamnya

Implementasikan logika:

Implementasikan logika yang terkait dengan setiap use case dalam metode-metode yang telah didefinisikan dalam kelas-kelas. Logika bisnis ini harus memastikan bahwa fungsionalitas yang diinginkan dalam setiap use case tercapai.

Tes dan validasi:

Setelah implementasi selesai, lakukan pengujian dan validasi untuk memastikan bahwa fungsionalitas yang diharapkan berjalan dengan baik dan sesuai dengan kebutuhan bisnis.


# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

- perbaiki class diagram

```mermaid
classDiagram
    class Rekening {
        -nomorRekening: String
        -pin: String
        -saldo: double
        -tagihan: Map<String, Double>
        -riwayatTransfer: List<String>
        +Rekening(nomorRekening: String, pin: String)
        +login(pin: String): boolean
        +getSaldo(): double
        +cekSaldo(namaNasabah: String): void
        +transfer(nomorRekeningTujuan: String, jumlahTransfer: double): boolean
        +isiSaldo(jumlahIsiSaldo: double): void
        +bayarTagihan(namaTagihan: String): boolean
        +tambahTagihan(namaTagihan: String, jumlahTagihan: double): void
        +getDaftarTagihan(): List<String>
        +getRiwayatTransfer(): List<String>
    }

    class Nasabah {
        -nama: String
        -alamat: String
        +Nasabah(nomorRekening: String, pin: String, nama: String, alamat: String)
        +getNama(): String
        +getAlamat(): String
        +cekSaldo(namaNasabah: String): void
        +transfer(nomorRekeningTujuan: String, jumlahTransfer: double): boolean
        +isiSaldo(jumlahIsiSaldo: double): void
        +bayarTagihan(namaTagihan: String): boolean
    }

    class BCAMobile {
        +main(args: String[]): void
        +transfer(scanner: Scanner, pengirim: Nasabah): void
        +eWallet(nasabah: Nasabah): void
        +transferViaOVO(scanner: Scanner, nasabah: Nasabah): void
        +transferViaDana(scanner: Scanner, nasabah: Nasabah): void
        +isiSaldo(scanner: Scanner, nasabah: Nasabah): void
        +tagihan(scanner: Scanner, nasabah: Nasabah): void
        +bayarTagihan(scanner: Scanner, nasabah: Nasabah): void
        +tambahTagihan(scanner: Scanner, nasabah: Nasabah): void
        +tampilkanTagihan(nasabah: Nasabah): void
        +tampilkanRiwayatTransfer(nasabah: Nasabah): void
    }

    class Scanner {
        +nextLine(): String
        +nextInt(): int
        +nextDouble(): double
        +close(): void
    }

    class OVO {
        +topUp(scanner: Scanner, nasabah: Nasabah): void
        +pay(scanner: Scanner, nasabah: Nasabah): void
    }

    class Dana {
        +deposit(scanner: Scanner, nasabah: Nasabah): void
        +transfer(scanner: Scanner, nasabah: Nasabah): void
    }

    class Tagihan {
        -namaTagihan: String
        -jumlahTagihan: double
        +Tagihan(namaTagihan: String, jumlahTagihan: double)
        +getNamaTagihan(): String
        +getJumlahTagihan(): double
        +setJumlahTagihan(jumlahTagihan: double): void
    }

    Rekening <|-- Nasabah
    BCAMobile o-- Scanner
    BCAMobile -- OVO
    BCAMobile -- Dana
    Rekening o-- Tagihan

```
untuk paragraf yang di bold adalah usecase yang telah di koding

| No. | Use Case                           | Prioritas | Penjelasan                                                                                                                                           |
| --- | ---------------------------------- | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1   | `Cek saldo`     | Tinggi- 100   | Cek saldo dan mutasi rekening memiliki prioritas tinggi karena ini adalah fitur dasar dari sebuah aplikasi perbankan yang paling sering digunakan. |
| 2   | `Transfer`                      | Tinggi-100    | Transfer dana juga memiliki prioritas tinggi karena fitur ini memungkinkan nasabah untuk mentransfer dana dengan mudah dan cepat.                       |
| 3   | `Pembayaran tagihan`                 | Tinggi-100    | Pembayaran tagihan juga memiliki prioritas tinggi karena fitur ini memungkinkan nasabah untuk membayar tagihan dengan mudah dan menghindari keterlambatan pembayaran. |
| 4   | `Top up e-wallet dan tranfer e-wallet`          | Tinggi-100    | Top up e-wallet dan pulsa memiliki prioritas tinggi karena fitur ini memungkinkan nasabah untuk mengisi ulang saldo e-wallet dan membeli pulsa.        |
| 5   | Investasi                          | Sedang-70    | Investasi memiliki prioritas sedang karena fitur ini menawarkan layanan investasi seperti pembelian reksa dana dan obligasi.                            |
| 6   | Pemesanan tiket                    | Sedang-70    | Pemesanan tiket memiliki prioritas sedang karena fitur ini menyediakan kemudahan bagi nasabah untuk memesan tiket pesawat, kereta api, atau hotel.     |
| 7   | Aktivasi kartu debit/kredit        | Rendah-40    | Aktivasi kartu debit/kredit memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah dan bukan fitur utama aplikasi perbankan.           |
| 8   | Pembukaan rekening baru            | Rendah-50    | Pembukaan rekening baru memiliki prioritas rendah karena proses ini hanya tersedia untuk nasabah yang sudah memiliki rekening di bank.                 |
| 9   | Permintaan cek/buku tabungan       | Rendah-40    | Permintaan cek/buku tabungan memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                          |
| 10  | Permintaan kartu ATM/debit baru    | Rendah-50    | Permintaan kartu ATM/debit baru memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                             |
| 11  | Pembayaran angsuran kredit         | Rendah-30    | Pembayaran angsuran kredit memiliki prioritas rendah karena fitur ini hanya relevan bagi nasabah yang memiliki kredit.                                |
| 12  | Pengaturan notifikasi dan pemberitahuan | Sedang-70 | Pengaturan notifikasi dan pemberitahuan memiliki prioritas sedang karena fitur ini memungkinkan nasabah mengelola pengaturan pemberitahuan.            |
| 13  | Penggantian PIN                    | Rendah-50    | Penggantian PIN memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                                             |
| 14  | Pembayaran online                  | Sedang-70    | Pembayaran online memiliki prioritas sedang karena fitur ini memungkinkan nasabah untuk melakukan pembayaran secara online.                            |
| 15  | Penarikan tunai di ATM             | Rendah-50    | Penarikan tunai di ATM memiliki prioritas rendah karena fitur ini umumnya dapat dilakukan di mesin ATM langsung.                                      |
| 16  | Pengiriman uang                    | Rendah-50    | Pengiriman uang memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                                             |
| 17  | Pembukaan deposito                 | Rendah-50    | Pembukaan deposito memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                                    |
| 18  | Penjadwalan transfer                | Rendah-30    | Penjadwalan transfer memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                                   |
| 19  | Penukaran mata uang                | Rendah-30    | Penukaran mata uang memiliki prioritas rendah karena fitur ini jarang digunakan oleh nasabah.                                                          |
| 20  | Pembelian asuransi                 | Rendah-30    | Pembelian asuransi memiliki prioritas rendah karena fitur ini tidak sering digunakan oleh nasabah.                                                     |


# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)
- salah link
https://youtu.be/mMkhsGPhF9Y

# 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

- tampilan

![dokumentasi](dokumentasi/tampilan.png)

- cek saldo

![dokumentasi](dokumentasi/cek-saldo.png)

- transfer

![dokumentasi](dokumentasi/transfer.png)

- ewallet ovo

![dokumentasi](dokumentasi/ewallet-ovo.png)

- ewallet dana

![dokumentasi](dokumentasi/ewallet-dana.png)

- isi saldo

![dokumentasi](dokumentasi/isi-saldo.png)

- tagihan

![dokumentasi](dokumentasi/tagihan.png)

- bayar tagihan

![dokumentasi](dokumentasi/bayar-tagihan.png)

- riwayat

![dokumentasi](dokumentasi/riwayat.png)


